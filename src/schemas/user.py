from datetime import datetime
from typing import Optional

from pydantic import UUID4, EmailStr

from src.schemas.base import CamelCaseSchema


class UserBase(CamelCaseSchema):
    name: Optional[str] = None
    email: Optional[EmailStr] = None


class UserCreate(UserBase):
    name: str
    email: EmailStr


class UserUpdate(UserBase):
    pass


class UserInDB(UserBase):
    uid: UUID4
    created_at: Optional[datetime] = None
    updated_at: Optional[datetime] = None

    class Config:
        orm_mode = True


class User(UserInDB):
    pass
