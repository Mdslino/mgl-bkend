from sqlalchemy import Column, String

from src.db.base_class import BaseModel


class User(BaseModel):
    name = Column(String, nullable=False)
    email = Column(String, nullable=False, unique=True)

    def __str__(self):
        return f"{self.name} - {self.email}"

    def __repr__(self):
        return f"<User {self.name} - {self.email}>"
