from datetime import datetime
from typing import Any
from uuid import uuid4

from sqlalchemy import Column, DateTime, Integer
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import as_declarative, declared_attr


@as_declarative()
class Base:
    id: Any
    __name__: str

    # Generate __tablename__ automatically
    @declared_attr
    def __tablename__(cls) -> str:
        return cls.__name__.lower()


class BaseModel(Base):
    __abstract__ = True

    id = Column(
        Integer, primary_key=True, index=True, unique=True, autoincrement=True
    )
    uid = Column(UUID(as_uuid=True), unique=True, index=True, default=uuid4)
    created_at = Column(DateTime(timezone=True), default=datetime)
    update_at = Column(
        DateTime(timezone=True), nullable=True, default=None, onupdate=datetime
    )
