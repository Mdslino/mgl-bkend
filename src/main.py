from fastapi import FastAPI

app = FastAPI()


@app.get("/healthcheck")
@app.head("/healthcheck")
def healthcheck():
    return {"status": "ok"}


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8000)
